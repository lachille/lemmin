#include "../includes/lem_in.h"

void	init_choose_path(t_path *path, int opt)
{
	int i;
	int j;

	path->c_line = 0;
	path->i_res = 0;
	path->buf_c_line = path->res_line;
	path->room_used_buf = ROOM_USED_BUF_START;
	path->room_used_len = 0;
	i = -1;
	if(!(path->couples = malloc(sizeof(int *) * path->buf_c_line)))
		print_error(MALLOC_FAIL, opt);
	if(!(path->room_used = malloc(sizeof(int) * ROOM_USED_BUF_START)))
		print_error(MALLOC_FAIL, opt);
	if(!(path->c_len = malloc(sizeof(int) * path->buf_c_line)))
		print_error(MALLOC_FAIL, opt);
	while (++i < path->buf_c_line)
	{
		if(!(path->couples[i] = malloc(sizeof(int) * (path->nb_max_path + 2))))
			print_error(MALLOC_FAIL, opt);
		path->c_len[i] = -1;
		j = -1;
		while (++j < path->nb_max_path + 2)
			path->couples[i][j] = -1;
	}
	i = -1;
	while (++i < ROOM_USED_BUF_START)
		path->room_used[i] = -1;
}

void	reset_room_used(t_path *path)
{
	int i;

	i = -1;
	while (++i < path->room_used_buf)
		path->room_used[i] = -1;
	path->room_used_len = 0;
}

/*
** Create the couples of path that we can take simultaneously

prendre id chemin trouver les autre possibilite :
si au moins 1 chemin compatible reset et incremente le start
des recherche de chemin compatible
lorsque aucun chemin compatible ou start == path->cur_line_res
incrementer id du premier chemin
*/
void	fill_couples(t_path *path, int opt)
{
	int id_first_path;
	int id_is_compatible;
	int start_is_compatible;
	int nb_add;


	start_is_compatible = 0;
	id_first_path = 0;
	while (id_first_path < path->cur_line_res)// && --stop)
	{
		nb_add = 1;
		add_path(path, id_first_path, opt);
		id_is_compatible = start_is_compatible;
		while (id_is_compatible < path->cur_line_res)
		{
			if (add_path(path, id_is_compatible, opt))
			{
				nb_add++;                                                                                                                                                                                                                                                            
				start_is_compatible = id_is_compatible;
			}
			id_is_compatible++;
		}
		path->c_len[path->c_line] = nb_add;
		if (nb_add < 2)
		{
			id_first_path++;
			start_is_compatible = id_first_path;
			if (path->c_line > 0
				&& path->couples[path->c_line - 1][0] == path->couples[path->c_line][0])
			{
				path->couples[path->c_line][0] = -1;
				path->c_line--;
			}
		}
		else
			start_is_compatible++;
		reset_room_used(path);
		if (++path->c_line == path->buf_c_line)
			double_couple(path, opt);
	}
}

void	choose_path_main(t_info *info)
{
	init_choose_path(info->path, info->option);
	fill_couples(info->path, info->option);
}
