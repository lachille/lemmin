#include "../includes/lem_in.h"

void	add_link(t_info *info, int i_line, int i_path, int link)
{
	if (i_path >= info->path->path_buf[i_line])
		double_path_buf(info->path, i_line, info->option);
	info->path->path[i_line][i_path] = link;
}

void	cpy_current_path(t_info *info, int i_line, int i_path, int link)
{
	int	*line_path_cpy;
	int i;

	i = -1;

	if (!(line_path_cpy = ft_intdup(info->path->path[i_line], info->path->path_buf[i_line])))  //check if i_path ok and no i_path - 1
		print_error(MALLOC_FAIL, info->option);
	while (++i < info->path->line_buf && info->path->path[i][0] != -1);
	if (i == info->path->line_buf)
		double_line(info);
	free(info->path->path[i]);
	info->path->path[i] = line_path_cpy;
	info->path->path_buf[i] = info->path->path_buf[i_line];
	add_link(info, i, i_path - 1, link);
}

/*
**	Check if we don't already have the room(link) we want to add in the creating path
**	if it's OK return the index where we can add the new room (link)
*/
int			check_double_link(t_info *info, int link, int i_line)
{
	int i;

	i = -1;
	while (++i < info->path->path_buf[i_line] && info->path->path[i_line][i] != -1)
	{
		if (info->path->path[i_line][i] == link)
			return (0);
	}
	return (i);
}
