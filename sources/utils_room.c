#include "../includes/lem_in.h"

int				check_double_room_name(t_room *rooms, int i)
{
	if (!(ft_strcmp(rooms->name[rooms->i], rooms->name[i])))
		return (1);
	return (0);
}

int				check_double_coord(t_room *rooms, int i)
{
	if (rooms->coord[rooms->i][0] == rooms->coord[i][0]
	&& rooms->coord[rooms->i][1] == rooms->coord[i][1])
		return (1);
	return (0);
}

