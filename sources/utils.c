#include "../includes/lem_in.h"

/*
** extract a piece in a string
** from begin to end include
*/
char	*extract_str(int begin, int end, char *str, int opt)
{
	char	*res;
	int		i;

	i = 0;
	begin -= 1;
	if (!(res = ft_strnew(end - begin)))
		print_error(MALLOC_FAIL, opt);
	while (++begin <= end)
		res[i++] = str[begin];
	return (res);
}

void	skip_commentary(char *str, int *index)
{
	if (str[*index] && str[*index + 1] && str[*index] == '#'
		&& str[*index + 1] != '#')
	{
		while (str[*index])
		{
			if (*index && str[*index - 1] == '\n'
				&& (str[*index] != '#' || str[*index + 1] == '#'))
				return ;
			*index += 1;
		}
	}
}

/*
**	Detect if it's a command or a COMMENTARY
**	opt is for Error message if !opt = Error else opt = Warning (just for -v opt)
*/
int		detect_command(char *str, int *index, int opt)
{
	int		i;
	char	*tmp;

	if (str[*index] && str[*index + 1] && str[*index] == '#'
		&& str[*index + 1] == '#')
	{
		*index += 2;
		i = *index;
		while (str[*index] && str[*index] != '\n')
			*index += 1;
		tmp = extract_str(i, *index - 1, str, opt);
		*index += 1;
		if (!ft_strcmp(tmp, "start"))
		{
			free(tmp);
			return (START);
		}
		i = ft_strcmp(tmp, "end");
		free(tmp);
		if (!i)
			return (END);
		else
			return (UNKNOWN);
	}
	else if (str[*index] && str[*index + 1] && str[*index] == '#'
		&& str[*index + 1] != '#')
	{
		skip_commentary(str, index);
		return (COMMENTARY);
	}
	return (NO_COMMAND);
}

void	print_tabc(char **tab, int len)
{
	int i;

	printf("ENTRER DANS PRINT TAB CHAR\n");
	i = -1;
	while (++i <= len)
	{
		printf("%s\n", tab[i]);
	}
}

void	print_tab_coor(int **tab, int len)
{
	int i;
	int j;

	printf("ENTRER DANS PRINT TAB INT\n");
	i = -1;
	while (++i <= len)
	{
		j = -1;
		while (++j < 2)
			printf("%d ", tab[i][j]);
		printf("\n");
	}
}


void	print_links(t_room *room)
{
	int i;
	int j;

	i = -1;
	while(++i < room->size_tab)
	{
		j = -1;
		printf("\e[4mroom %d (%s)	\e[0m->", i, room->name[i]);
		while (++j < room->links_lenght[i])
		{
			printf("| ");
			i % 2 ? printf("\e[1;49;33m") : printf("\e[1;49;36m");
			printf("%d->\e[32m", j);
			room->links[i][j] != -1 ? printf("\e[32m") : printf("\e[31m");
			printf("%d", room->links[i][j]);
			printf("\e[0m|");
		}
		printf("\n\e[90m/////////////////////////////////////////////////////////////////////////////\e[0m\n");
	}
}

void	print_path(t_path *path)
{
	int i;
	int j;

	i = -1;
	while(++i < path->line_buf)
	{
		printf("line_buf = %d", path->line_buf);

		j = -1;
		printf("\n\e[4mPATH %d	\e[0m->", i);
		while (++j < path->path_buf[i])
		{

				printf("| ");
				i % 2 ? printf("\e[1;49;33m") : printf("\e[1;49;36m");
				printf("%d->\e[32m", j);
				path->path[i][j] != -1 ? printf("\e[32m") : printf("\e[31m");
				path->path[i][j] == -3 ? printf("\e[34m") : 0;
					printf("%d", path->path[i][j]);
				printf("\e[0m|");
		}
		printf("\n\e[90m/////////////////////////////////////////////////////////////////////////////\e[0m\n");
	}

}

void	print_path_res(t_path *path)
{
	int i;
	int j;

	i = -1;
	while(++i < path->res_line)
	{
		j = -1;
		printf("\n\e[4mPATH %d | len:%d	\e[0m->", i, path->res_len[i]);
		while (++j <= path->res_len[i])
		{
				printf("| ");
				i % 2 ? printf("\e[1;49;33m") : printf("\e[1;49;36m");
				printf("%d->\e[32m", j);
					printf("%d", path->path_res[i][j]);
				printf("\e[0m|");
		}
		printf("\n\e[90m/////////////////////////////////////////////////////////////////////////////\e[0m\n");
	}
}
