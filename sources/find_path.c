#include "../includes/lem_in.h"

void		init_path(t_info *info, int opt)
{
	int i;
	int j;

	i = -1;
	info->path->line_buf = PATH_LENGHT_START;
	info->path->res_line = PATH_LENGHT_START;
	info->path->cur_line_res = 0;
	if (!(info->path->path = malloc(sizeof(int *) * PATH_LENGHT_START))
	|| !(info->path->path_res = malloc(sizeof(int *) * PATH_LENGHT_START))
	|| !(info->path->path_buf = malloc(sizeof(int) * PATH_LENGHT_START))
	|| !(info->path->path_len = malloc(sizeof(int) * PATH_LENGHT_START))
	|| !(info->path->res_len = malloc(sizeof(int) * PATH_LENGHT_START)))
		print_error(MALLOC_FAIL, opt);
	while (++i < PATH_LENGHT_START)
	{
		j = -1;
		info->path->path_buf[i] = PATH_LENGHT_START;
		if (!(info->path->path[i] = malloc(sizeof(int) * info->path->path_buf[i])))
			print_error(MALLOC_FAIL, opt);
		info->path->res_len[i] = -1;
		while (++j < info->path->path_buf[i])
				info->path->path[i][j] = -1;
	}
}

/*
**	id is the last link of the current path
**	link is the new link we want to add
**	i_line is the current line in int **path
**
** here we have some bad choice of word link should be room and we check it's links to create a path
** add_link is the current room we checked and add it to the path we're creating
** 
** Here we're creating all the existing path possible valid or not
*/
void	fill_path(t_info *info, int id, int i_line)
{
	int count;
	int i;
	int i_path;

	i = -1;
	count = 0;
	while (++i < info->rooms->links_lenght[id] && info->rooms->links[id][i] != -1)
	{
		if ((i_path = check_double_link(info, info->rooms->links[id][i], i_line)))
		{
			if (count)
				cpy_current_path(info, i_line, i_path, info->rooms->links[id][i]);
			else
				add_link(info, i_line, i_path, info->rooms->links[id][i]);
			count++;
		}
	}
	if (!count)
		info->path->path[i_line][0] = -2;
}

/*
**	Here we're copying in int **path_res just the valid path in int **path
*/
void	fill_path_res(t_path *path, int line, int opt)
{
	int		**tmp;
	int		*tmpl;
	int		i;

	i = -1;
	if (path->cur_line_res == path->res_line)
	{
		if (!(tmp = malloc(sizeof(int *) * (path->res_line * 2))))
			print_error(MALLOC_FAIL, opt);
		if (!(tmpl = malloc(sizeof(int) * (path->res_line * 2))))
			print_error(MALLOC_FAIL, opt);
		while (++i < path->res_line * 2)
		{
			if (i < path->res_line)
			{
				tmp[i] = ft_intdup(path->path_res[i], path->res_len[i] + 1);
				tmpl[i] = path->res_len[i];
				free(path->path_res[i]);
			}
			else
				tmpl[i] = -1;
		}
		free(path->res_len);
		path->res_len = tmpl;
		free(path->path_res);
		path->path_res = tmp;
		path->res_line *= 2;
	}
	path->path_res[path->cur_line_res] = ft_intdup(path->path[line],  path->path_len[line] + 1);
	path->res_len[path->cur_line_res] = path->path_len[line];
	path->cur_line_res++;
}

/*
**	check if we have completed all path (valid or not)
*/
int		check_all_path_create(t_path *path)
{
	int i;

	i = -1;

	while (++i < path->line_buf && path->path[i][0] != -1)
		if (path->path[i][0] != -2 && path->path[i][0] != -3)
			return (1);
	return (0);
}

/*
** Here we try to remove wrong path of int **path
** for big map to not have to browse all the array
*/
void	refresh_path(t_info *info)
{
	int i;
	int j;
	int **tmp_path;
	int *tmp_buf;

	i = -1;
	j = 0;
	if (!(tmp_path = malloc(sizeof(int *) * info->path->line_buf)))
		print_error(MALLOC_FAIL, info->option);
	if (!(tmp_buf = malloc(sizeof(int) * info->path->line_buf)))
		print_error(MALLOC_FAIL, info->option);
	i = -1;
	while (++i < info->path->line_buf)
	{
		if (info->path->path[i][0] != -2)
		{
			tmp_path[j] = info->path->path[i];
			tmp_buf[j++] = info->path->path_buf[i];
		}
		else
			free(info->path->path[i]);

	}
	while (j < info->path->line_buf)
	{
		if (!(tmp_path[j] = malloc(sizeof(int) * 1)))
			print_error(MALLOC_FAIL, info->option);
		tmp_path[j][0] = -1;
		tmp_buf[j++] = 0;
	}
	free(info->path->path);
	free(info->path->path_buf);
	info->path->path_buf = tmp_buf;
	info->path->path = tmp_path;
}

void	path_main(t_info *info)
{
	int	id;
	int turn;
	int line;

	turn = 0;
	line = 0;
	init_path(info, info->option);
	info->path->path[0][0] = info->i_start;
	while (check_all_path_create(info->path))
	{
		if (((id = search_last_room(info->path, line)) >= 0) && id != info->i_end && info->path->path_len[line] == turn)
			fill_path(info, id, line);
		else if (id == info->i_end && info->path->path[line][0] != -3)
		{
			info->path->path[line][0] = -3;
			fill_path_res(info->path, line, info->option);
		}
		line++;
		if (line == info->path->line_buf || info->path->path[line][0] == -1)
		{
			turn++; 
			refresh_path(info);
			line = 0;
		}
	}
	if (info->option)
		print_path_res(info->path);
	if (!info->path->cur_line_res)
		print_error(NO_VALID_PATH, info->option);
}
