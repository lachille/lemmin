#include "../includes/lem_in.h"

/*
**	Check if the room line is valid
*/
static int		valid_room_line(char *str, int opt)
{
	int i;
	int count_space;

	count_space = 0;

	i = -1;
	while (str[++i])
	{
		if (i == 0 && str[i] == 'L')
			print_error(ROOM_L_NAME, opt);
		if (str[i] == ' ')
			count_space++;
		else if (str[i] == '-')
			return (0);
		else if ((!count_space && (!ft_isprint(str[i])))
		|| (count_space && !ft_isdigit(str[i])))
		{
				count_space ? print_error(ROOM_COORD_NAME, opt)
					: print_error(ROOM_NAME, opt);
		}
		if (count_space > 2)
			print_error(ROOM_SPACE, opt);
	}
	if (!count_space)
		return (0);
	else if (count_space != 2)
		print_error(ROOM_SPACE, opt);
	return (1);
}

/*
**	Double the space of char **info->rooms->name and int *[2]info->rooms->coord
*/
static void		realloc_name_n_coor(t_room *rooms, int opt)
{
	char	**tmpc;
	int		**tmpi;
	int		i;

	i = -1;
	if (!(tmpc = malloc(sizeof(char*) * rooms->buf_tab * 2)))
		print_error(MALLOC_FAIL, opt);
	if (!(tmpi = malloc(sizeof(int*) * rooms->buf_tab * 2)))
		print_error(MALLOC_FAIL, opt);
	while (++i < rooms->buf_tab)
	{

			tmpc[i] = ft_strdup(rooms->name[i]);
			tmpi[i] = ft_intdup(rooms->coord[i], 2);
			free(rooms->name[i]);
			free(rooms->coord[i]);
	}
	rooms->buf_tab *= 2;
	free(rooms->name);
	free(rooms->coord);
	rooms->name = tmpc;
	rooms->coord = tmpi;
}

/*
** Check if there is multiple instance of the same room or same coord
*/
static void		check_double_room(t_room *rooms, int opt)
{
	int	i;

	i = -1;
	while (++i < rooms->i)
	{
		if (check_double_coord(rooms, i))
		{
			if (check_double_room_name(rooms, i))
			{
				print_error(IDENTICAL_ROOM, opt);
				rooms->i -= 1;
				return ;
			}
			print_error(ROOM_COORD_DOUBLE, opt);
		}
	}
	i = -1;
	while (++i < rooms->i)
	{
		if (check_double_room_name(rooms, i))
		{
			if (check_double_coord(rooms, i))
				print_error(IDENTICAL_ROOM, opt);
			else
			{
				print_error(ROOM_NAME_DOUBLE, opt);
				rooms->coord[i][0] = rooms->coord[rooms->i][0];
				rooms->coord[i][1] = rooms->coord[rooms->i][1];
				free(rooms->coord[rooms->i]);
				free(rooms->name[rooms->i]);
			}
			rooms->i -= 1;
			return ;
		}
	}

}

/*
**	fill info->rooms->name and info->rooms->coord
*/
static void		fill_room(t_info *info, char *str)
{
	int i;
	int begin;
	char *tmp;

	i = -1;
	if (info->command == START)
	{
		if (info->i_start != -1)
			print_error(NEW_START, info->option);
		info->i_start = info->rooms->i;
	}
	else if (info->command == END)
	{
		if (info->i_end != -1)
			print_error(NEW_END, info->option);
		info->i_end = info->rooms->i;
	}

	if (!(info->rooms->coord[info->rooms->i] = malloc(sizeof(int) * 2)))
		print_error(MALLOC_FAIL, info->option);
	while (str[++i] != ' ');
	info->rooms->name[info->rooms->i] = extract_str(0, i - 1, str, info->option);
	begin = i + 1;
	while (str[++i] != ' ');
	info->rooms->coord[info->rooms->i][0] = atoi(tmp = extract_str(begin, i - 1, str, info->option));
	begin = i + 1;
	free(tmp);
	while (str[++i]);
	info->rooms->coord[info->rooms->i][1] = atoi(tmp = extract_str(begin, i - 1, str, info->option));
	free(tmp);
	if (info->rooms->i == info->rooms->buf_tab - 1)
		realloc_name_n_coor(info->rooms, info->option);
	check_double_room(info->rooms, info->option);
	info->rooms->i += 1;
}

/*
**	main of the pasrsing for the room
*/
void			parse_room(t_info *info)
{
	int		begin;
	char	*tmp;
	int		command;
	int retour;

	while (1)
	{
		if ((command = detect_command(info->map, &info->i_map, info->option)) == NO_COMMAND)
		{
			begin = info->i_map;
			while (info->map[++info->i_map] && info->map[info->i_map] != '\n');
			if (!(retour = valid_room_line(tmp = extract_str(begin, info->i_map - 1, info->map, info->option), info->option)))
			{
				info->i_map = begin;
				free(tmp);
				break;
			}
			info->i_map++;
			fill_room(info, tmp);
			free(tmp);
		}
		info->command = command;
	}
	info->rooms->size_tab = info->rooms->i;
	if (info->i_start < 0)
		print_error(NO_START, info->option);
	else if (info->i_end < 0)
		print_error(NO_END, info->option);
}
