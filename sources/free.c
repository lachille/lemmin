#include "../includes/lem_in.h"

static void    free_path(t_path *path)
{
    int i;

    i = -1;
    while (++i < path->line_buf)
        free(path->path[i]);
    free(path->path_buf);
    free(path->path_len);
    free(path->path);
        
}

static void    free_path_res(t_info *info)
{
    int i;

    i = -1;
    while (++i < info->path->cur_line_res)
        free(info->path->path_res[i]);
    free(info->path->path_res);
    free(info->path->res_len);
}

static void    free_room(t_info *info)
{
    int i;

    i = -1;
    while(++i < info->rooms->size_tab)
    {
		free(info->rooms->links[i]);
        free(info->rooms->name[i]);
        free(info->rooms->coord[i]);
    }
    free(info->rooms->name);
    free(info->rooms->links_lenght);
    free(info->rooms->links);
    free(info->rooms->coord);
    free(info->map);
}

static void     free_choose_path(t_info *info)
{
    int i;

    i = -1;
    while (++i < info->path->buf_c_line)
        free(info->path->couples[i]);
    free(info->path->couples);
	free(info->path->room_used);
	free(info->path->c_len);
}

static void     free_calculate_turn(t_info *info)
{
    int i;

    i = -1;
    while (++i < info->path->c_line)
	{
		free(info->path->nb_turn[i]);
		free(info->path->nb_ant_path[i]);
	}	
	free(info->path->nb_turn);
	free(info->path->nb_ant_path);


}

void    free_all(t_info *info)
{
    free_path(info->path);
    free_path_res(info);
    free_room(info);
    free_choose_path(info);
	free_calculate_turn(info);
}
