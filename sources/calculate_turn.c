#include "../includes/lem_in.h"

void	calculate_turn_init(t_path *path, int opt)
{
	int i;
	int j;


	i = -1;
	if (!(path->nb_turn = malloc(sizeof(int *) * (path->c_line + 1))))
		print_error(MALLOC_FAIL, opt);
	if (!(path->nb_ant_path = malloc(sizeof(int *) * (path->c_line + 1))))
		print_error(MALLOC_FAIL, opt);
	while (++i < path->c_line)
	{
		if (!(path->nb_turn[i] = malloc(sizeof(int) * path->c_len[i])))
			print_error(MALLOC_FAIL, opt);
		if (!(path->nb_ant_path[i] = malloc(sizeof(int) * path->c_len[i])))
			print_error(MALLOC_FAIL, opt);
		j = -1;
		while (++j < path->c_len[i])
		{
			path->nb_turn[i][j] = -1;
			path->nb_ant_path[i][j] = 0;
		}
	}
}

void	print_turn(t_path *path)
{
	int i;
	int index;

	i = -1;
	while (++i < path->c_line)
	{
		index = -1;
		printf("couples  : ");
		while (++index < path->c_len[i])
			printf("%d ", path->couples[i][index]);
		index = -1;
		printf("\nlen      : ");
		while (++index < path->c_len[i])
			printf("%d ", path->res_len[path->couples[i][index]]);
		index = -1;
		printf("\nant_path : ");
		while (++index < path->c_len[i])
			printf("%d ", path->nb_ant_path[i][index]);
		index = -1;
		printf("\nnb_turn  : ");
		while (++index < path->c_len[i])
			printf("%d ", path->nb_turn[i][index]);
		printf("\n\n");
	}
	printf("best choice is n'%d\n", path->i_res);

}

/*
**	nb_turn is a copy of path_res but with the number of turn for each couple
**	path->nb_turn[i][0] = (path->res_len[path->couples[i][0]] - 1) + nb_ants; 
**	is the initialisation where we put all the ants on the first path and leave the other one empty for one combo

*/
void	calculate_turn(t_path *path, int nb_ants) 
{
	int i;
	int j;
	int k;

	i = -1;
	while (++i < path->c_line)
	{
		path->nb_turn[i][0] = (path->res_len[path->couples[i][0]] - 1) + nb_ants;
		path->nb_ant_path[i][0] = nb_ants;
		j = 1;
		while (j < path->c_len[i])
		{
			//search max
			k = -1;
			while (++k < j)
			{
				if (path->nb_turn[i][k] > path->nb_turn[i][k + 1])
					break;		
			}
			path->nb_turn[i][j] = !path->nb_ant_path[i][j] ? 0 :  (path->res_len[path->couples[i][j]] - 1) + path->nb_ant_path[i][j];
			if ((path->res_len[path->couples[i][k]] - 1) + path->nb_ant_path[i][k] - 1 >= (path->res_len[path->couples[i][j]] - 1) + path->nb_ant_path[i][j] + 1)
			{
				path->nb_ant_path[i][k] -= 1;
				path->nb_ant_path[i][j] += 1;
				path->nb_turn[i][k] = !path->nb_ant_path[i][k] ? 0 :  (path->res_len[path->couples[i][k]] - 1) + path->nb_ant_path[i][k];
				path->nb_turn[i][j] = !path->nb_ant_path[i][j] ? 0 :  (path->res_len[path->couples[i][j]] - 1) + path->nb_ant_path[i][j];
			}
			else
				j++;
		}
	}
}

void	choose_best_couple(t_path *path)
{
	int i;

	i = -1;
	path->i_res = 0;
	while (++i < path->c_line)
	{
		if (path->nb_turn[i][0] < path->nb_turn[path->i_res][0])
			path->i_res = i;
	}
}

void	calculate_turn_main(t_info *info)
{
	// Turn = (Len - 1) + nb_Ants
	calculate_turn_init(info->path, info->option);
	calculate_turn(info->path, info->nb_ants);
	choose_best_couple(info->path);
	print_turn(info->path);
}
