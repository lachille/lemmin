#include "../includes/lem_in.h"

/*
**  fill char *info->map with given map
*/
static void	fill_map(t_info *info)
{
    int i;
    char *buf;

    if (!(buf = ft_strnew(SIZE_BUF))
        || !(info->map = ft_strnew(0)))
        print_error(MALLOC_FAIL, info->option);
    while ((i = read(0, buf, SIZE_BUF)) > 0)
	{
		buf[i] = '\0';
		if (!(info->map = ft_strjoin_free(info->map, buf, 1)))
            print_error(MALLOC_FAIL, info->option);
	}
	if (i == -1)
		print_error(READ_FAIL, info->option);
    free(buf);
}

/*
**  option available : -v (verbose)
*/
static void	chk_opt(int ac, char **av, t_info *info)
{
    if (ac > 1)
    {
        if (!ft_strcmp(av[1], "-v"))
            info->option = 1;
        else
        {
            print_error(BAD_ARG, info->option);
            exit (1);
        }
    }
    else
        info->option = 0;
}

int			main(int ac, char **av)
{
    t_info info;
    t_room rooms;
    t_path path;

    info.rooms = &rooms;
    info.path = &path;

    chk_opt(ac, av, &info);
    fill_map(&info);
    main_parsing(&info);
    path_main(&info);
    choose_path_main(&info);
    calculate_turn_main(&info);
    print_res_main(&info);
    free_all(&info);
    return (0);
}
