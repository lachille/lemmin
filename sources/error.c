#include "../includes/lem_in.h"
#include "../includes/error.h"

void    print_error_room(int error, int opt)
{
	if (error > W_ROOM_BEGIN && error < W_ROOM_END)
	{
		if (opt)
		{
			ft_putstr_fd("\e[93mWarning during the parsing of the rooms\n\e[0;4mReason : ", 2);
			ft_putstr_fd(g_errors[error], 2);
			ft_putstr_fd("\e[0m\n", 2);
		}
	}
	else
	{
		ft_putstr_fd("\e[31mError during the parsing of the rooms\n\e[0;4mReason : ", 2);
		ft_putstr_fd(g_errors[error], 2);
		ft_putstr_fd("\e[0m\n", 2);
		exit (1);
	}
}

void    print_error_link(int error, int opt)
{
	if (error > W_LINK_BEGIN && error < W_LINK_END)
	{
		if (opt)
		{
			ft_putstr_fd("\e[93mWarning during the parsing of the links\n\e[0;4mReason : ", 2);
			ft_putstr_fd(g_errors[error], 2);
			ft_putstr_fd("\e[0m\n", 2);
		}
	}
	else
	{
		ft_putstr_fd("\e[31mError during the parsing of the links\n\e[0;4mReason : ", 2);
		ft_putstr_fd(g_errors[error], 2);
		ft_putstr_fd("\e[0m\n", 2);
		exit (1);
	}
}

void    print_error_path(int error)
{
	ft_putstr_fd("\e[31mError during the creation of path\n\e[0;4mReason : ", 2);
	ft_putstr_fd(g_errors[error], 2);
	ft_putstr_fd("\e[0m\n", 2);
	exit (1);
}

void    print_error_parsing(int error, int opt)
{
	if (error > E_ROOM_BEGIN && error < E_ROOM_END)
		print_error_room(error, opt);
	else if (error > E_LINK_BEGIN && error < E_LINK_END)
		print_error_link(error, opt);
	else
	{
		ft_putstr_fd("\e[31mError parsing\n\e[0;4m", 2);
		ft_putstr_fd(g_errors[error], 2);
		exit (1);
	}
}

void    print_error_system_call(int error)
{
	ft_putstr_fd("\e[31mError system call\n\e[0;4mReason : ", 2);
	ft_putstr_fd(g_errors[error], 2);
	ft_putstr_fd("\n", 2);
	exit (1);
}

void    print_error(int error, int opt)
{
	if (error > PARSING_BEGIN && error < PARSING_END)
		print_error_parsing(error, opt);
	else if (error > PATH_BEGIN && error < PATH_END)
		print_error_path(error);
	else if (error > S_CALL_BEGIN && error < S_CALL_END)
		print_error_system_call(error);
}