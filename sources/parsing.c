#include "../includes/lem_in.h"

void	init_info(t_info *info)
{
	info->nb_ants = 0;
	info->i_start = -1;
	info->i_end = -1;
	info->i_map = 0;
	info->rooms->i = 0;
	info->rooms->buf_tab = 16;
	if (!(info->rooms->name = malloc(sizeof(char *) * info->rooms->buf_tab)))
		print_error(MALLOC_FAIL, info->option);
	if (!(info->rooms->coord = malloc(sizeof(int *) * info->rooms->buf_tab)))
		print_error(MALLOC_FAIL, info->option);
}

/*
**	extrait le nombre de fourmis
**	info->i_map est l'index pour parcourir info->map
*/
void	search_ants(t_info *info)
{
	int		begin;
	int		end;
	char	*tmp;
	
	begin = 0;
	while (detect_command(info->map, &begin, info->option) != NO_COMMAND);
	end = begin;
	while (ft_isdigit(info->map[end]))
		end++;
	if (info->map[end] != '\n')
	{
		print_error(ANTS_NOT_DEFINE, info->option);
		exit (1);
	}
	info->nb_ants = ft_atoi(tmp = extract_str(begin, end - 1, info->map, info->option));
	info->i_map = end + 1;
	free(tmp);
}

int     main_parsing(t_info *info)
{
	init_info(info);
	search_ants(info);
	parse_room(info);
	link_main(info);
	return (0);
}
