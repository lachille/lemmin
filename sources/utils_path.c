#include "../includes/lem_in.h"

void		double_path_buf(t_path *path, int i_line, int opt)
{
	int *res;
	int i;

	i = -1;
	if (!(res = malloc(sizeof(int *) * path->path_buf[i_line] * 2)))
		print_error(MALLOC_FAIL, opt);
	while (++i < path->path_buf[i_line] * 2)
	{
		if (i < path->path_buf[i_line])
			res[i] = path->path[i_line][i];
		else
			res[i] = -1;
	}
	path->path_buf[i_line] *= 2;
	free(path->path[i_line]);
	path->path[i_line] = res;
}

static int	double_line3(t_path *path, int *tab, int i)
{
	if (i < path->line_buf / 2)
		return (tab[i]);
	return (-1);	
}

static void	double_line2(t_info *info, int **tmp_path, int *tmp_len, int *tmp_buf)
{
	int i;
	int j;

	i = -1;
	while (++i < info->path->line_buf)
	{
		if (!(tmp_path[i] = malloc(sizeof(int) * (i < info->path->line_buf / 2 
		? info->path->path_buf[i] : PATH_LENGHT_START))))
			print_error(MALLOC_FAIL, info->option);
		tmp_len[i] = double_line3(info->path, info->path->path_len, i);
		tmp_buf[i] = double_line3(info->path, info->path->path_buf, i);
		tmp_buf[i] == -1 ? tmp_buf[i] = PATH_LENGHT_START : 0;
		j = -1;
		while (++j < (i < info->path->line_buf / 2 
		? info->path->path_buf[i] : PATH_LENGHT_START))
			tmp_path[i][j] = i < info->path->line_buf / 2 ? info->path->path[i][j] : -1;
		if (i < info->path->line_buf / 2)
			free(info->path->path[i]);
	}
}

void		double_line(t_info *info)
{
	int	**tmp_path;
	int	*tmp_len;
	int	*tmp_buf;

	info->path->line_buf *= 2;
	if(!(tmp_path = malloc(sizeof(int *) * info->path->line_buf))
	|| !(tmp_len = malloc(sizeof(int) * info->path->line_buf))
	|| !(tmp_buf = malloc(sizeof(int) * info->path->line_buf)))
		print_error(MALLOC_FAIL, info->option);
	double_line2(info, tmp_path, tmp_len, tmp_buf);
	free(info->path->path);
	free(info->path->path_len);
	free(info->path->path_buf);
	info->path->path_len = tmp_len;
	info->path->path_buf = tmp_buf;
	info->path->path = tmp_path;
}

int			search_last_room(t_path *path, int line)
{
	int i;

	i = 0;
	while (i < path->path_buf[line] && path->path[line][i] != -1)
		i++;
	path->path_len[line] = i - 1;
	if (!i)
		return (-1);
	return (path->path[line][i - 1]);
}
