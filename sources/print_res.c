#include "../includes/lem_in.h"


void	init_print_res(t_info *info)
{
	int i;
	int j;

	i = -1;
	if (!(info->path->ant_on_path = malloc(sizeof(int *) * (info->nb_ants))))
		print_error(MALLOC_FAIL, info->option);
	while (++i < info->nb_ants)
	{
		if (!(info->path->ant_on_path[i] = malloc(sizeof(int) * 2)))
			print_error(MALLOC_FAIL, info->option);
		j = -1;
		while (++j < 2)
			info->path->ant_on_path[i][j] = -1;
	}
}

void	fill_ant_on_path(t_info *info)
{
	int i;
	int j;

	i = -1;
	while (++i < info->path->c_len[info->path->i_res] && info->path->nb_ant_path[info->path->i_res][i] > 0);
	info->path->c_len[info->path->i_res] = i;
	i = 0;
	j = -1;
	while (i < info->nb_ants && info->path->nb_ant_path[info->path->i_res][0])
	{
		if (++j == info->path->c_len[info->path->i_res])
			j = 0;
		if (info->path->nb_ant_path[info->path->i_res][j])
		{
			info->path->nb_ant_path[info->path->i_res][j] -= 1;
			info->path->ant_on_path[i][0] = info->path->couples[info->path->i_res][j];
			info->path->ant_on_path[i][1] = 0;
			i++;
		}		
	}
}

int chk_move_forward(t_info *info, int j)
{
	int i;
	int test;

	test = 1;
	i = -1;
	while (++i < j)
	{
		if (info->path->ant_on_path[i][0] == info->path->ant_on_path[j][0]
		&& info->path->ant_on_path[i][1] == info->path->ant_on_path[j][1] + 1)
			test = 0;
	}
	return (test);
}

/*
**  choose couple[i_res] -> combo de optimal
**	c_len[i_res] = nb of path
**	nb_ant_path[i_res]
**	
**	create ant_on_path (parcourir nb_ant_path enlever 1 fourmis et lui assigne le path dans ant_on_path)
**	ant_on_path[x][0] -> path emprunte
**	ant_on_path[x][1] -> index dans le path actuel
**	ant_on_path[x] -> x + 1 = ID fourmis 
*/
void	print_current_state(t_info *info)
{
	int i;
	int j;
	int test;
	int turn = 0;
	test = 1;
	while (test)
	{
		i = 0;
		test = 0;
		while (i < info->nb_ants && info->path->ant_on_path[i][1])
			i++;
		i += info->path->c_len[info->path->i_res];
		j = -1;
		while (++j < info->nb_ants && j < i)
		{
			if (chk_move_forward(info, j))
				info->path->ant_on_path[j][1] += 1;
			if (info->path->ant_on_path[j][0] != -1 && info->path->ant_on_path[j][1] 
			<= info->path->res_len[info->path->ant_on_path[j][0]])
			{
				if (info->path->ant_on_path[j][1])
				{
					if (info->option)
						printf("L%d-%s index=%d   ", j, info->rooms->name[info->path->path_res[info->path->ant_on_path[j][0]][info->path->ant_on_path[j][1]]], info->path->ant_on_path[j][1]);
				}
				test = 1;
			}
		}
		turn++;
		if (info->option)
			printf("\n");
	}
	printf("nb turn = %d\n", --turn);
}

void	free_print_res(t_info *info)
{
	int i;

	i = -1;
	while (++i < info->nb_ants)
		free(info->path->ant_on_path[i]);
	free(info->path->ant_on_path);
}

void	print_res_main(t_info *info)
{
	init_print_res(info);
	fill_ant_on_path(info);
	print_current_state(info);
	free_print_res(info);
}