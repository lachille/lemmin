#include "../includes/lem_in.h"

void		init_link(t_room *room, int opt)
{
	int i;
	int j;

	i = -1;
	if (!(room->links = malloc(sizeof(int *) * room->size_tab)))
		print_error(MALLOC_FAIL, opt);
	if (!(room->links_lenght = malloc(sizeof(int) * room->size_tab)))
		print_error(MALLOC_FAIL, opt);
	while (++i < room->size_tab)
	{
		j = -1;
		room->links_lenght[i] = LINK_LENGHT_START;
		if (!(room->links[i] = malloc(sizeof(int) * room->links_lenght[i])))
			print_error(MALLOC_FAIL, opt);
		while (++j < room->links_lenght[i])
				room->links[i][j] = -1;
	}
	room->i = 0;
}

void		double_size_int_tab(t_room *room, int line, int opt)
{
	int *res;
	int i;

	i = -1;
	if (!(res = malloc(sizeof(int *) * room->links_lenght[line] * 2)))
		print_error(MALLOC_FAIL, opt);
	while (++i < room->links_lenght[line] * 2)
	{
		if (i < room->links_lenght[line])
			res[i] = room->links[line][i];
		else
			res[i] = -1;

	}
	room->links_lenght[line] *= 2;
	free(room->links[line]);
	room->links[line] = res;
}

int			search_last_link(t_room *room, int room_a, int room_b)
{
	int i;

	i = 0;
	while (i < room->links_lenght[room_a] && room->links[room_a][i] != -1)
	{
		if (room->links[room_a][i] == room_b)
			return (-1);
		i++;
	}
	if (i == room->links_lenght[room_a])
		return (-2);
	return(i);
}

/*
**	int i_a -> index room a 
**	int i_b -> index room b
*/
void		fill_link(t_room *room, int i_a, int i_b, int opt)
{
	int i;
	int j;

		i = search_last_link(room, i_a, i_b);
		if (i == -2)
		{
			double_size_int_tab(room, i_a, opt);
			i = search_last_link(room, i_a, i_b);
		}
		if (i >= 0)
			room->links[i_a][i] = i_b;

		j = search_last_link(room, i_b, i_a);
		if (j == -2)
		{
			double_size_int_tab(room, i_b, opt);
			j = search_last_link(room, i_b, i_a);
		}
		if (j >= 0)
			room->links[i_b][j] = i_a;
		if (i == -1 || j == -1)
			print_error(LINK_DOUBLE, opt);

		
}

int			search_i_room(t_room *room, char *str)
{
	int i;

	i = -1;
	while (++i < room->size_tab)
	{
		if (!ft_strcmp(str, room->name[i]))
			return (i);
	}
	return (-1);
}

static int	extract_link(t_room *room, char *str, int opt)
{
	int		i;
	int		room_a;
	int		room_b;
	char	*tmp;
	int		begin;

	i = -1;
	room_a = -1;
	room_b = -1;
	while (str[++i])
	{
		if (str[i] == '-')
		{
			room_a = search_i_room(room, tmp = extract_str(0, i - 1, str, opt));
			free(tmp);
			begin = i + 1;
		}
	}
	room_b = search_i_room(room, tmp = extract_str(begin, i - 1, str, opt));
	free(tmp);
	if (room_a == -1 || room_b == -1)
	{
		print_error(ROOM_NOT_FOUND, opt);
		return (0);
	}
	fill_link(room, room_a, room_b, opt);

	return (1);
}

/*
**	check if the line is valid for a link
*/
static int	valid_link_line(char *str, int opt)
{
	int i;
	int count_union;

	count_union = 0;
	i = -1;
	while (str[++i])
	{
		if (str[0] == '-' || (!str[i + 1] && str[i] == '-'))
		{
			print_error(MISSPLACED_UNION, opt);
			return (0);
		}
		else if (str[i] == '-')
			count_union++;
		else if (!ft_isprint(str[i]) || str[i] == ' ')
				print_error(LINK_NAME, opt);
	}
	if (!str[0])
		return (0);
	else if (count_union != 1)
	{
		print_error(NB_UNION, opt);
		return (0);
	}
	return (1);
}

/*
** give the lenght (the number) of how rooms are link to this one
*/
void		set_len_links(t_room *rooms)
{
	int i;
	int j;

	i = -1;
	while (++i < rooms->size_tab)
	{
		j = -1;
		while (++j < rooms->links_lenght[i] && rooms->links[i][j] != -1);
		rooms->links_lenght[i] = j;
	}
}

void		link_main(t_info *info)
{
	int		begin;
	char *tmp;


	init_link(info->rooms, info->option);
	while (1)
	{
		if (detect_command(info->map, &info->i_map, info->option) == NO_COMMAND)
		{
			begin = info->i_map;
			while (info->map[++info->i_map] && info->map[info->i_map] != '\n');
			if (!(valid_link_line(tmp = extract_str(begin, info->i_map - 1, 
			info->map, info->option), info->option)) || !extract_link(info->rooms, tmp, info->option))
			{
				if (info->map[info->i_map])
				{
					print_error(STOP_BEFORE_END, info->option);
				}
				free(tmp);
				break;
			}
			info->map[info->i_map + 1] ? info->i_map++ : 0;
			free(tmp);
		}
	}

	print_links(info->rooms);
	set_len_links(info->rooms);
	info->path->nb_max_path = info->rooms->links_lenght[info->i_start]
	<= info->rooms->links_lenght[info->i_end] ? info->rooms->links_lenght[info->i_start] :
	info->rooms->links_lenght[info->i_end];
}
