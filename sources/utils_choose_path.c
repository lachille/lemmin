#include "../includes/lem_in.h"

void		double_room_used_buf(t_path *path, int opt)
{
	int *tmp;
	int i;

	i = -1;
	if (!(tmp = malloc(sizeof(int) * (path->room_used_buf * 2))))
		print_error(MALLOC_FAIL, opt);
	while (++i < path->room_used_buf * 2)
	{
		if (i < path->room_used_buf)
			tmp[i] = path->room_used[i];
		else
			tmp[i] = -1;
	}
	path->room_used_buf *= 2;
	free(path->room_used);
	path->room_used = tmp;
}

int		check_doubles(t_path *path, int line_to_add)
{
	int i;
	int j;

	i = -1;
	while (++i < path->room_used_buf && path->room_used[i] != -1)
	{
		j = 0;
		while (++j < path->res_len[line_to_add])
		{
			if (path->path_res[line_to_add][j] == path->room_used[i])
			{
				return (0);
			}
		}
	}
	return (1);
}

int		add_path(t_path *path, int line_to_add, int opt)
{
	int i;
	int j;
	int tmp;

	j = 0;
	i = 0;
	if ((tmp = check_doubles(path, line_to_add)))
	{
		while (path->room_used_len + (path->res_len[line_to_add]) >= path->room_used_buf)
			double_room_used_buf(path, opt);
		while (++j < path->res_len[line_to_add])
			path->room_used[path->room_used_len + j - 1] = path->path_res[line_to_add][j];
		path->room_used_len += j - 1;
		while (i < path->nb_max_path + 1 && path->couples[path->c_line][i] != -1)
			i++;
		path->couples[path->c_line][i] = line_to_add;
	}
	return(tmp);
}

void	double_couple(t_path *path, int opt)
{
	int **tmp;
	int	*tmp_len;
	int i;
	int j;

	i = -1;
	if(!(tmp = malloc(sizeof(int *) * path->buf_c_line * 2)))
		print_error(MALLOC_FAIL, opt);
	if(!(tmp_len = malloc(sizeof(int) * path->buf_c_line * 2)))
		print_error(MALLOC_FAIL, opt);
	while (++i < path->buf_c_line * 2)
	{
		if (i < path->buf_c_line)
		{
			tmp[i] = ft_intdup(path->couples[i], path->nb_max_path + 2);
			tmp_len[i] = path->c_len[i];
			free(path->couples[i]);
		}
		else
		{
			if(!(tmp[i] = malloc(sizeof(int) * (path->nb_max_path + 2))))
				print_error(MALLOC_FAIL, opt);
			tmp_len[i] = -1;
			j = -1;
			while (++j < path->nb_max_path + 2)
				tmp[i][j] = -1;
		}
	}
	path->buf_c_line *=2;
	free(path->couples);
	free(path->c_len);
	path->couples = tmp;
	path->c_len = tmp_len;
}

