# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lachille <lachille@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/26 17:11:30 by santa             #+#    #+#              #
#    Updated: 2020/03/06 13:49:25 by lachille         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem_in

CC = gcc
CFLAGS = -Wall -Wextra -Werror
AR = ar rc
RM = rm -rf
	
LIBFT = libft
INCDIR = includes
OBJDIR = objects
SRCDIR = sources

SOURCES = 	main.c \
			error.c \
			parsing.c \
			room_parsing.c \
			utils_room.c \
			link_parsing.c \
			find_path.c \
			utils_path.c \
			utils_path2.c \
			choose_path.c \
			utils_choose_path.c \
			calculate_turn.c \
			print_res.c \
			utils.c \
			free.c \

			

INC =	lem_in.h 

SRCS = $(addprefix $(SRCDIR)/, $(SOURCES))
OBJS = $(addprefix $(OBJDIR)/, $(SOURCES:.c=.o))


all : $(OBJDIR) $(NAME)

$(NAME) : $(INCDIR)/$(INC) $(OBJS) 
	make -C $(LIBFT)
	$(CC) $(CFLAGS) $(OBJS) $(LIBFT)/libft.a -o $(NAME)

$(OBJDIR) :
	mkdir -p objects

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -I $(INCDIR) -c $< -o $@

clean :
	$(RM) $(OBJS)
	make -C $(LIBFT) clean

fclean : clean
	$(RM) $(NAME)
	make -C $(LIBFT) fclean

re : fclean all

debug: $(INCDIR)/$(INC)
	make -C $(LIBFT)
	$(CC) $(CFLAGS) -g3 $(SRCS) $(LIBFT)/libft.a

debugS: $(INCDIR)/$(INC)
	make -C $(LIBFT)
	$(CC) $(CFLAGS) -g3 -fsanitize=address $(SRCS) $(LIBFT)/libft.a
