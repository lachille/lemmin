#ifndef ERROR_H
# define ERROR_H

const char	g_errors[S_CALL_END][128] = {
	[0] = "Unexpected error",
	[ANTS_NOT_DEFINE] = "Number of ants wasn't define\n",
	[BAD_ARG] = "Bad arguments, use -v for verbose (print warning)\n",
	/*
	** Room Errors
	*/
	[ROOM_SPACE] = "Bad number of spaces, must be 2",
	[ROOM_NAME] = "Room name has non printable character",
	[ROOM_L_NAME] = "Name of the room begin by 'L'",
	[ROOM_COORD_NAME] = "Non digit character in the coordinates",
	[ROOM_COORD_DOUBLE] = "The same coordinates were found for 2 different rooms",
	[NO_START] = "START not define",
	[NO_END] = "END not define",
	/*
	** Room Warnings
	*/
	[NEW_START] = "New START found, it will be actualised",
	[NEW_END] = "New END found, it will be actualised",
	[ROOM_NAME_DOUBLE] = "The same room was found for 2 different coordinates, it will be actualised",
	[IDENTICAL_ROOM] = "A double of a room was found, this line will be ignored",
	/*
	** Link Errors
	*/
	[MISSPLACED_UNION] = "Missplaced '-' -> \"-xxxxx\" or \"xxxxx-\"",
	[LINK_NAME] = "Invalid caracter in the line",
	[NB_UNION] = "Only one '-' should be in the line",
	[ROOM_NOT_FOUND] = "A room wasn't found in the line",
	[STOP_BEFORE_END] = "Parsing of the links stopped before the end of the file",
	/*
	** Link Warnings
	*/
	[LINK_DOUBLE] = "Link already detected, it will be ignored",
	/*
	** Path Errors
	*/
	[NO_VALID_PATH] = "No valid path found",
	/*
	** System call Errors
	*/
	[READ_FAIL] = "Error READ",
	[MALLOC_FAIL] = "Error MALLOC",
};

#endif