/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lachille <lachille@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 00:18:39 by gbikoumo          #+#    #+#             */
/*   Updated: 2020/03/07 11:29:51 by lachille         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "../libft/libft.h"
// # include "error.h"
# include <stdio.h>
# include <unistd.h>

# define SIZE_BUF 1024
# define LINK_LENGHT_START 4
# define PATH_LENGHT_START 4
# define ROOM_USED_BUF_START 16

typedef	struct	s_room
{
	int		buf_tab;
	int		size_tab;
	int		i;
	char    **name;
	//init at -1
	int     **links;
	int		*links_lenght;
	int     **coord;
	// int     *Ant_in_room;
	// int		*ants_in_end;
}				t_room;


/*
**	-1 : non initiate
**	tab[0] = -2 : dead path
**
*/
typedef	struct	s_path
{
	//path construct
	int		**path;
	int		*path_buf;
	int		*path_len;
	int		line_buf;
	//res (chemin valide)
	int		**path_res;
	int		*res_len;
	int		res_line;
	int		cur_line_res;
	// find path
	int		nb_max_path;
	int		**couples;
	int		*c_len;
	int		**nb_ant_path;
	int 	**nb_turn;
	int		c_line;
	int		buf_c_line;
	int		*room_used;
	int		room_used_len;
	int		room_used_buf;
	int		i_res;
	int		**ant_on_path;
}
				t_path;

typedef	struct	s_info
{
	int		command;
	int		nb_ants;
	int		i_start;
	int		i_end;
	char	*map;
	int		i_map;
	int		option;
	t_path	*path;
	t_room	*rooms; 
}				t_info;


typedef	enum	e_error {
	/*
	** Parsing error
	*/
	PARSING_BEGIN,
		ANTS_NOT_DEFINE,
		BAD_ARG,
		/*
		** Room errors and warnings
		*/
		E_ROOM_BEGIN,
			ROOM_SPACE,
			ROOM_NAME,
			ROOM_L_NAME,
			ROOM_COORD_NAME,
			ROOM_COORD_DOUBLE,
			NO_START,
			NO_END,
			/*
			** Room warnings
			*/
			W_ROOM_BEGIN,
				NEW_START,
				NEW_END,
				ROOM_NAME_DOUBLE,
				IDENTICAL_ROOM,
			W_ROOM_END,
		E_ROOM_END,
		/*
		** Link errors and warnings
		*/
		E_LINK_BEGIN,
			MISSPLACED_UNION,
			LINK_NAME,
			NB_UNION,
			ROOM_NOT_FOUND,
			/*
			** Link warnings
			*/
			W_LINK_BEGIN,
				LINK_DOUBLE,
				STOP_BEFORE_END,
			W_LINK_END,
		E_LINK_END,


	PARSING_END,
	/*
	** Path error
	*/
	PATH_BEGIN,
		NO_VALID_PATH,
	PATH_END,
	/*
	** System call error
	*/
	S_CALL_BEGIN,
		READ_FAIL,
		MALLOC_FAIL,
	S_CALL_END,


}				t_error;

typedef	enum	e_command {
	NO_COMMAND,
	UNKNOWN,
	COMMENTARY,
	START,
	END,

}				t_command;



/*
**	error.c
*/
void    print_error(int error, int opt);
/*
**	utils.c
*/
char	*extract_str(int begin, int end, char *str, int opt);
void	skip_commentary(char *str, int *index);
int		detect_command(char *str, int *index, int opt);
void	print_tabc(char **tab, int len);
void	print_tab_coor(int **tab, int len);
void	print_links(t_room *room);
void	print_path(t_path *path);
void	print_path_res(t_path *path);

/*
**	utils_room.c
*/
int		check_double_room_name(t_room *rooms, int i);
int		check_double_coord(t_room *rooms, int i);
/*
**	utils_path.c
*/
void	double_path_buf(t_path *path, int i_line, int opt);
void	double_line(t_info *info);
int		search_last_room(t_path *path, int line);
/*
**	utils_path2.c
*/
void	add_link(t_info *info, int i_line, int i_path, int link);
void	cpy_current_path(t_info *info, int i_line, int i_path, int link);
int		check_double_link(t_info *info, int link, int i_line);
/*
**	utils_choose_path.c
*/
int		check_doubles(t_path *path, int line_to_add);
void	double_couple(t_path *path, int opt);
int	add_path(t_path *path, int line_to_add, int opt);

/*
**	parsing.c
*/
int     main_parsing(t_info *info);
/*
**	room_parsing.c
*/
void	parse_room(t_info *info);
/*
**	link_parsing.c
*/
void	link_main(t_info *info);
/*
**	find_path.c
*/
void	path_main(t_info *info);
/*
**	choose_path.c
*/
void	choose_path_main(t_info *info);
/*
**	calculate_turn.c
*/
void	calculate_turn_main(t_info *info);
/*
**	print_res.c
*/
void	print_res_main(t_info *info);
/*
**	free.c
*/
void    free_all(t_info *info);

#endif
