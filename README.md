# lem-in

Lem-in is 42 subject where a map is randomly generated with some rooms and connections. 
The goal is to find the best solution for what paths the ants should go. This solution can differ in function of the ants number.

Read the [subject](https://gitlab.com/lachille/lemmin/-/blob/master/subject) for mor detail.


## Authors

- [@Leonard ACHILLE](https://gitlab.com/lachille)


## Deployment

To deploy this project run

```bash
  make
```

```bash
  ./lem_in < Map/"a map"
```
or with verbose
```bash
  ./lem_in < Map/"a map" -v
```

You can generate new map with generator, to see the option:
```bash
  ./generator --help
```


## Features

- Modular error handling see [error.h](https://gitlab.com/lachille/lemmin/-/blob/master/includes/error.h)
